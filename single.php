<?php get_header(); ?>
	<main id="content" class="content">

<?php while ( have_posts() ) : the_post(); ?>

			
			<article class="post page" id="pageid-<?php the_ID(); ?>">
				
								        
            <div class = "col1">

                <h1><?php echo get_field("nom"); ?></h1>

                <p> Taille :    <?php echo get_field("taille"); ?> toise </p>
            

             
                <p><?php echo get_field("description"); ?></p>
            </div>   

            <div class="col2">
                <img src=" <?php  echo get_field('image'); ?>" alt="<?php echo get_field("nom"); ?>">
				
                <?php
                    $featured_posts = get_field('mange');
                    if( $featured_posts ): ?>
                    
                <ul> Grignotte :
                  <?php foreach( $featured_posts as $post ): 

                     // Setup this post for WP functions (variable must be named $post).
                  setup_postdata($post); ?>
                    <li class="a">
                        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                    </li>
                    <?php endforeach; ?>
                </ul>
                    <?php 
                    // Reset the global post object so that the rest of the page works correctly.
                     wp_reset_postdata(); ?>
                 <?php endif; ?>
            </div>  
       
			</article>
            <?php  endwhile; ?>
		
        </main>

 <?php get_footer(); ?>