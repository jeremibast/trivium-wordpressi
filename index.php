<?php get_header(); ?>

<div class="main">

    <h1> Index</h1>

   
    <?php 
    $args = array(
        'orderby' => 'title',
        'order'   => 'ASC',
    );
    $query = new WP_Query( $args );
  
    if ( $query->have_posts() ) : 
        $lettre = "A";
        ?><div class="cat"> <?php
         
        
        while ( $query->have_posts() ) : $query->the_post(); ?>
            
               <?php

                    if ($lettre != substr(get_the_title(), 0, 1)) {
                     $lettre = substr(get_the_title(), 0, 1);
                     ?> <h2><ul><?php echo $lettre; ?></ul></h2>
                  <?php } ?>

                       
                    <li class="a">
                <?php $title = get_field("nom");
                the_shortlink( $title ); ?> 
                </li>
            
     <?php 
        endwhile; ?>
        </div> <?php
    else: 
        _e( 'Sorry, no pages matched your criteria.', 'textdomain' ); 
    endif; 
    ?>
</div>



<?php get_footer(); ?>

